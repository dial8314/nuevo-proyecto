CREATE DATABASE tiendabarrio;
USE tiendabarrio;
CREATE TABLE IF NOT EXISTS usuarios(
id INT NOT NULL AUTO_INCREMENT,
usuario VARCHAR (16) NOT NULL,
nombre VARCHAR (80) NOT NULL,
clave VARCHAR(16) NOT NULL,
fecha_nacimiento DATE,
correo VARCHAR(250) NOT NULL,
activo BOOLEAN  DEFAULT false,
PRIMARY KEY (id,usuario)) ENGINE = Innodb;

